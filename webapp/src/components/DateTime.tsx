type Props = Pick<Intl.DateTimeFormatOptions, 'dateStyle' | 'timeStyle'> & {
	timestamp: Date
}

const full = new Intl.DateTimeFormat([], {
	timeStyle: 'full',
	dateStyle: 'full',
})

const DateTime = ({ timestamp, dateStyle, timeStyle }: Props) => {
	const format = new Intl.DateTimeFormat([], {
		dateStyle,
		timeStyle,
	})
	return (
		<time title={full.format(timestamp)} dateTime={timestamp.toISOString()}>
			{format.format(timestamp)}
		</time>
	)
}

export default DateTime
