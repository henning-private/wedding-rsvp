import { useEffect, useState } from 'react'
import { CSSTransition, TransitionGroup } from 'react-transition-group'
import './ImageSlider.css'

type Props = {
	readonly images: string[]
	readonly interval?: number
}

const ImageSlider = ({ images, interval = 5000 }: Props) => {
	const [index, setIndex] = useState(0)
	useEffect(() => {
		const id = setInterval(() => {
			setIndex((i) => (i + 1) % images.length)
		}, interval)

		return () => clearInterval(id)
	}, [images, interval])

	return (
		<figure className='image image-slider'>
			<TransitionGroup component={null}>
				<CSSTransition
					key={images[index]}
					timeout={1000}
					classNames='fade'
				>
					<img src={images[index]} />
				</CSSTransition>
			</TransitionGroup>
		</figure>
	)
}

export default ImageSlider
