import { ComponentStory, ComponentMeta } from '@storybook/react'

import ImageSlider from './ImageSlider'

export default {
	title: 'Components/ImageSlider',
	component: ImageSlider,
	decorators: [
		(Story) => (
			<div className='box' style={{ width: '512px', height: '512px' }}>
				<Story />
			</div>
		),
	],
} as ComponentMeta<typeof ImageSlider>

const Template: ComponentStory<typeof ImageSlider> = (args) => (
	<ImageSlider {...args} />
)

export const Primary = Template.bind({})
Primary.args = {
	images: [
		'https://place-hold.it/256',
		'https://place-hold.it/256/f00/fff&text=Test',
		'https://place-hold.it/256/00f/fff&text=Blabla&fontsize=24',
	],
}
