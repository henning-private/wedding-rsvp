import { render, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { useTextInput } from './TextInput'

describe('TextInput', () => {
	test('Basic interactions', async () => {
		const user = userEvent.setup()

		const Testee = ({ cb }: { cb: (v: string) => void }) => {
			const [value, , TextInput] = useTextInput({
				label: 'LABEL',
				placeholder: 'PLACEHOLDER',
			})
			return (
				<>
					{TextInput}
					<button type='button' onClick={() => cb(value)}>
						Click
					</button>
					<p data-testid='output'>{value}</p>
				</>
			)
		}

		let value = ''
		render(<Testee cb={(v) => (value = v)} />)

		await user.click(screen.getByRole('textbox'))
		await user.keyboard('test')
		await user.click(screen.getByRole('button'))

		expect(value).toBe('test')
	})
})
