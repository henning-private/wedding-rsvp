import classNames from 'classnames'
import { ReactElement, useState } from 'react'

type Props = {
	readonly label: string
	readonly placeholder: string
	readonly initialValue?: string
	readonly validate?: (value: string | undefined) => boolean
	readonly icon?: ReactElement
}

export const useTextInput = ({
	label,
	placeholder,
	initialValue,
	validate,
	icon,
}: Props): [string, (v: string) => void, ReactElement] => {
	const [value, setValue] = useState(initialValue ?? '')
	const isValid = validate?.call(undefined, value)

	return [
		value,
		setValue,
		// eslint-disable-next-line react/jsx-key
		<div className='field'>
			<label className='label'>{label}</label>
			<div
				className={classNames('control', {
					'has-icons-left': icon,
					'has-icons-right': validate,
				})}
			>
				<input
					className={classNames('input', {
						'is-success': isValid === true,
						'is-danger': isValid === false,
					})}
					type='text'
					placeholder={placeholder}
					value={value}
					onChange={(e) => setValue(e.target.value)}
				/>
				{icon ? (
					<span className='icon is-small is-left'>{icon}</span>
				) : null}

				{isValid ? (
					<span className='icon is-small is-right'>
						<i className='fa-solid fa-check' />
					</span>
				) : null}
			</div>
			{isValid === true ? (
				<p className='help is-success'>{`${label} is valid`}</p>
			) : isValid === false ? (
				<p className='help is-danger'>{`${label} is invalid`}</p>
			) : null}
		</div>,
	]
}
