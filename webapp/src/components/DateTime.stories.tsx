import { ComponentStory, ComponentMeta } from '@storybook/react'

import DateTime from './DateTime'

export default {
	title: 'Components/DateTime',
	component: DateTime,
	argTypes: {
		timestamp: { control: 'date' },
		timeStyle: {
			control: 'select',
			options: ['full', 'long', 'medium', 'short', undefined],
		},
		dateStyle: {
			control: 'select',
			options: ['full', 'long', 'medium', 'short', undefined],
		},
	},
} as ComponentMeta<typeof DateTime>

// eslint-disable-next-line react/prop-types
const Template: ComponentStory<typeof DateTime> = ({ timestamp, ...args }) => (
	<DateTime
		timestamp={
			typeof timestamp !== 'object' ? new Date(timestamp) : timestamp
		}
		{...args}
	/>
)

const timestamp = new Date('Jun 8, 2022, 12:19 PM')

export const Default = Template.bind({})
Default.args = {
	timestamp,
}

export const OnlyTime = Template.bind({})
OnlyTime.args = {
	timestamp,
	timeStyle: 'full',
}

export const OnlyDate = Template.bind({})
OnlyDate.args = {
	timestamp,
	dateStyle: 'full',
}

export const Full = Template.bind({})
Full.args = {
	timestamp,
	dateStyle: 'full',
	timeStyle: 'full',
}
