import { PropsWithChildren, useEffect, useState } from 'react'
import { createPortal } from 'react-dom'

type PortalProps = PropsWithChildren<{
	rootName?: string
}>

// from https://stackoverflow.com/a/55142289/2156840
const Portal = ({ children, rootName = 'modal-root' }: PortalProps) => {
	const [modalContainer] = useState(document.createElement('div'))
	useEffect(() => {
		// Find the root element in your DOM
		let modalRoot = document.getElementById(rootName)
		// If there is no root then create one
		if (!modalRoot) {
			const tempEl = document.createElement('div')
			tempEl.id = rootName
			document.body.append(tempEl)
			modalRoot = tempEl
		}
		// Append modal container to root
		modalRoot.appendChild(modalContainer)
		return () => {
			// On cleanup remove the modal container
			modalRoot?.removeChild(modalContainer)
		}
	}, []) // <- The empty array tells react to apply the effect on mount/unmount

	return createPortal(children, modalContainer)
}

export default Portal
