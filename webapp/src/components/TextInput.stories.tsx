import { useTextInput } from './TextInput'

export default {
	title: 'Components/TextInput',
}

export const Simple = () => {
	const [, , TextInput] = useTextInput({
		label: 'Label',
		placeholder: 'Placeholder',
	})

	return TextInput
}

export const WithIcon = () => {
	const [, , TextInput] = useTextInput({
		label: 'Label',
		placeholder: 'Placeholder',
		icon: <i className='fa-solid fa-user'></i>,
	})

	return TextInput
}

export const WithValidation = () => {
	const [, , TextInput] = useTextInput({
		label: 'Label',
		placeholder: 'Placeholder',
		initialValue: 'initial',
		validate: (val) => !!val?.length,
	})

	return TextInput
}

export const Full = () => {
	const [, , TextInput] = useTextInput({
		label: 'Label',
		placeholder: 'Placeholder',
		initialValue: 'initial',
		validate: (val) => !!val?.length,
		icon: <i className='fa-solid fa-user'></i>,
	})

	return TextInput
}

export const Multiple = () => {
	const [value_1, , TextInput_1] = useTextInput({
		label: '1',
		placeholder: 'Placeholder',
		initialValue: 'initial',
		validate: (val) => !!val?.length,
		icon: <i className='fa-solid fa-user'></i>,
	})
	const [value_2, , TextInput_2] = useTextInput({
		label: '2',
		placeholder: 'Placeholder',
		initialValue: 'initial',
		validate: (val) => !!val?.length,
		icon: <i className='fa-solid fa-user'></i>,
	})

	return (
		<>
			{TextInput_1}
			{TextInput_2}
			<p>{`Value 1: ${value_1}`}</p>
			<p>{`Value 2: ${value_2}`}</p>
		</>
	)
}
