import { PropsWithChildren } from 'react'
import css from './Section.module.css'

type Props = PropsWithChildren<{
	readonly title: string
}>

const Section = ({ children, title }: Props) => {
	return (
		<div className={css.text}>
			<section className='hero'>
				<div className='hero-body'>
					<div className='container has-text-centered'>
						<p className='title is-1'>{title}</p>
					</div>
				</div>
			</section>
			<section className='section'>
				<div className='container'>{children}</div>
			</section>
		</div>
	)
}

export default Section
