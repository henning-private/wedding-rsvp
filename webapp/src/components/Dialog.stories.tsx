import { ComponentStory, ComponentMeta } from '@storybook/react'

import Dialog from './Dialog'

export default {
	title: 'Components/Dialog',
	component: Dialog,
	argTypes: {
		isActive: { control: 'boolean' },
		title: { control: 'text' },
	},
} as ComponentMeta<typeof Dialog>

const Template: ComponentStory<typeof Dialog> = (args) => <Dialog {...args} />

export const Primary = Template.bind({})
Primary.args = {
	isActive: true,
	title: 'Test',
	children: <div>Content</div>,
	footer: (
		<>
			<button className='button is-primary'>Ok</button>
			<button className='button'>Cancel</button>
		</>
	),
	onClose: () => {
		/* NOTHING */
	},
}

export const WithoutCloseButton = Template.bind({})
WithoutCloseButton.args = {
	isActive: true,
	title: 'No close button',
	children: <div>Content</div>,
	footer: (
		<>
			<button className='button is-primary'>Ok</button>
			<button className='button'>Cancel</button>
		</>
	),
}
