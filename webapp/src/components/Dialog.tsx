import classNames from 'classnames'
import { PropsWithChildren, ReactNode, memo } from 'react'
import Portal from './Portal'

type DialogProps = PropsWithChildren<{
	title: string
	isActive: boolean
	onClose?: () => Promise<void> | void
	footer?: ReactNode
}>

const Dialog = memo(function Dialog({
	isActive,
	onClose,
	children,
	title,
	footer,
}: DialogProps) {
	const dialog = (
		<div className={classNames('modal', { 'is-active': isActive })}>
			<div className='modal-background' onClick={onClose} />
			<div className='modal-card'>
				<header className='modal-card-head'>
					<p className='modal-card-title'>{title}</p>
					{onClose ? (
						<button
							className='delete'
							aria-label='close'
							type='button'
							onClick={onClose}
						/>
					) : null}
				</header>
				<section className='modal-card-body'>{children}</section>
				{footer ? (
					<footer className='modal-card-foot'>{footer}</footer>
				) : null}
			</div>
		</div>
	)

	return <Portal>{isActive ? dialog : null}</Portal>
})

export default Dialog
