import { ComponentStory, ComponentMeta } from '@storybook/react'

import Section from './Section'
import lorem from '../loremipsum'

export default {
	title: 'Components/Section',
	component: Section,
} as ComponentMeta<typeof Section>

const Template: ComponentStory<typeof Section> = (args) => <Section {...args} />

export const Primary = Template.bind({})
Primary.args = {
	title: 'Hier koennte Ihr Titel stehen',
	children: <div>{lorem}</div>,
}
