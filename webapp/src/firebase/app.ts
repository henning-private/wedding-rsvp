import { initializeApp } from 'firebase/app'
import { initializeAppCheck, ReCaptchaV3Provider } from 'firebase/app-check'

// Your web app's Firebase configuration
const firebaseConfig = {
	apiKey: 'AIzaSyARN68NUS6UzRjYOQlJi5LBSaUrElxqDgc',
	authDomain: 'wedding-rsvp-96597.firebaseapp.com',
	projectId: 'wedding-rsvp-96597',
	storageBucket: 'wedding-rsvp-96597.appspot.com',
	messagingSenderId: '143449733871',
	appId: '1:143449733871:web:eb306bc5b4bcf11a46791a',
}

// Initialize Firebase
export const app = initializeApp(firebaseConfig)


if (process.env.NODE_ENV === 'development') {
	// @ts-expect-error -- See: https://firebase.google.com/docs/app-check/web/debug-provider?authuser=0&hl=en
	self.FIREBASE_APPCHECK_DEBUG_TOKEN = true;
}

initializeAppCheck(app, {
	provider: new ReCaptchaV3Provider(
		'6LepBlggAAAAADtNByd4_T7jD6cYpRPPc-L3VQUL',
	),
	isTokenAutoRefreshEnabled: true,
})
