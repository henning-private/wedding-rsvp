import {
	getAuth,
	getRedirectResult,
	browserSessionPersistence,
	signInWithPopup,
	GoogleAuthProvider,
} from 'firebase/auth'
import { collection, setDoc, doc, serverTimestamp } from 'firebase/firestore'
import { app } from './app'
import { db } from './firestore'

// Initialize Firebase Authentication and get a reference to the service
export const auth = getAuth(app)

auth.useDeviceLanguage()

await auth.setPersistence(browserSessionPersistence)

const cred = await getRedirectResult(auth)
if (cred) {
	await setDoc(doc(collection(db, 'users'), cred.user.uid), {
		last_login: serverTimestamp(),
	}, { merge: true })
}

const provider = new GoogleAuthProvider()
export const signIn = () => signInWithPopup(auth, provider)
