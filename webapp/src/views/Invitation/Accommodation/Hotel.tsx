type HotelProps = {
	name: string
	adress: string
	imgUrl: string
	websiteUrl: string
	mapsUrl: string
}

const Hotel = ({ name, adress, imgUrl, websiteUrl, mapsUrl }: HotelProps) => (
	<div className='card' data-aos='fade'>
		<header className='card-header'>
			<p className='card-header-title'>{name}</p>
		</header>
		<div className='card-image'>
			<figure className='image is-5by3'>
				<img src={imgUrl} />
			</figure>
		</div>
		<div className='card-content'>
			<div className='content'>
				<p>
					<span className='icon-text'>
						<span className='icon'>
							<i className='fa-solid fa-location-dot' />
						</span>
						<a
							href={mapsUrl}
							rel='noopener noreferrer'
							target='_blank'
						>
							{adress}
						</a>
					</span>
				</p>

				<p>
					<span className='icon-text'>
						<span className='icon'>
							<i className='fa-solid fa-earth-europe' />
						</span>
						<a
							href={websiteUrl}
							rel='noopener noreferrer'
							target='_blank'
						>
							{new URL(websiteUrl).hostname}
						</a>
					</span>
				</p>
			</div>
		</div>
	</div>
)

export default Hotel
