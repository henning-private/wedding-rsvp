import Section from '../../../components/Section'
import schleikate from './schleikate_5by3.jpg'
import alte_fischereischule from './alte_fischereischule.jpg'
import angler_hof from './angler_hof.jpg'
import beachside from './beachside.jpg'
import heldts_hotel from './heldts_hotel.jpg'
import kappelner_hof from './kappelner_hof.jpg'
import schleibruecke from './schleibruecke.jpg'
import seelust from './seelust.jpg'
import sieseby from './sieseby.jpg'
import stadthotel_eck from './stadthotel_eck.jpg'
import Hotel from './Hotel'
import './title.css'

const hotels: Parameters<typeof Hotel>[0][] = [
	{
		name: 'Schlei Kate',
		adress: 'Am Noor 6, 24392 Boren',
		imgUrl: schleikate,
		mapsUrl: 'https://g.page/Schleikate?share',
		websiteUrl: 'https://schleikate.de/gaestezimmer/',
	},
	{
		name: 'Zur Schleibrücke Lindaunis',
		adress: 'Schleistraße 3, 24392 Boren',
		imgUrl: schleibruecke,
		mapsUrl: 'https://goo.gl/maps/N2pPSCztsqXh31Lo6',
		websiteUrl: 'http://www.zurschleibruecke.de/pension.html',
	},
	{
		name: 'Hotel Angler Hof',
		adress: 'Große Str. 25, 24392 Süderbrarup',
		imgUrl: angler_hof,
		mapsUrl: 'https://goo.gl/maps/fPqJJegg8WkcSbRE8',
		websiteUrl: 'https://www.anglerhof.de/zimmer/',
	},
	{
		name: 'Gasthof Alt Sieseby von 1867',
		adress: 'Dorfstraße 24, 24351 Thumby',
		imgUrl: sieseby,
		mapsUrl: 'https://goo.gl/maps/NsE3jqZkDwXvhXY5A',
		websiteUrl: 'https://gasthof-alt-sieseby.de/das-hotel/',
	},
	{
		name: 'Hotel Restaurant Kappelner Hof',
		adress: 'Wassermühlenstraße 1, 24376 Kappeln',
		imgUrl: kappelner_hof,
		mapsUrl: 'https://goo.gl/maps/4sqE4FemsWGrrdkb7',
		websiteUrl: 'https://www.kappelner-hof.de/',
	},
	{
		name: 'Stadthotel Eckernförde',
		adress: 'Am Exer 3, 24340 Eckernförde',
		imgUrl: stadthotel_eck,
		mapsUrl: 'https://goo.gl/maps/19xVBgTVLv5VD2oi6',
		websiteUrl:
			'https://www.stadthotel-eckernfoerde.de/hotel-infos/hotel-beschreibung',
	},
	{
		name: 'Hotel Alte Fischereischule',
		adress: 'Sehestedter Str. 77, 24340 Eckernförde',
		imgUrl: alte_fischereischule,
		mapsUrl: 'https://goo.gl/maps/j2kU6Pg2QttmpJsa6',
		websiteUrl: 'https://www.hotel-alte-fischereischule.de/zimmer',
	},
	{
		name: 'Heldts Hotel',
		adress: 'Berliner Str. 10, 24340 Eckernförde',
		imgUrl: heldts_hotel,
		mapsUrl: 'https://goo.gl/maps/A7uqmXVfjYemCzRv7',
		websiteUrl: 'https://www.apartmenthotel.heldt-eckernfoerde.de/',
	},
	{
		name: 'BeachSide',
		adress: 'Berliner Str. 71-73, 24340 Eckernförde',
		imgUrl: beachside,
		mapsUrl: 'https://g.page/BeachSideEckernfoerderStrand?share',
		websiteUrl: 'https://www.beachside.de/40.html',
	},
	{
		name: 'Hotel Seelust',
		adress: 'Preußerstraße 3, 24340 Eckernförde',
		imgUrl: seelust,
		mapsUrl: 'https://goo.gl/maps/dXfkwqDHDWQxhHn36',
		websiteUrl:
			'https://www.seelust-hotel.de/hotel-infos/hotel-beschreibung',
	},
]

const Accommodation = () => (
	<>
		<div className='accommodation-title'>
			<section className='hero is-large'>
				<div className='hero-body'>
					<div className='container has-text-centered'>
						<h1 className='title'>Unterkunft</h1>
					</div>
				</div>
			</section>
		</div>
		<Section title='Wo kann ich unterkommen?'>
			<div>
				<p className='title'>
					Braucht Ihr Hilfe bei der Suche nach einer Unterkunft,
					sprich uns bitte auch gerne an!
				</p>

				<div className='columns is-multiline'>
					{hotels.map((h) => (
						<div key={h.name} className='column is-one-third'>
							<Hotel {...h} />
						</div>
					))}
				</div>
			</div>
		</Section>
	</>
)

export default Accommodation
