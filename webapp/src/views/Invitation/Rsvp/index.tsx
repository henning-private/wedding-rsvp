import { Party as PartyType, Member as MemberType } from '../../../database'
import Accepted from './Accepted'
import Declined from './Declined'
import New from './New'
import Section from '../../../components/Section'
import { useState } from 'react'
import classNames from 'classnames'

type Props = {
	readonly party: PartyType
	readonly accept: () => Promise<void>
	readonly decline: () => Promise<void>
	readonly updateMembers: (
		members: MemberType[],
		finish: boolean,
	) => Promise<void>
	readonly reopen: () => Promise<void>
}

const calcStatus = ({ attending, finished }: PartyType) => {
	return {
		hasAnswered: attending != undefined,
		isAttending: attending === true,
		isNotAttending: attending === false,
		isFinished: finished === true,
	}
}

const Finished = ({
	plural,
	reopen,
}: {
	plural: boolean
	reopen: () => Promise<void>
}) => {
	const [working, setWorking] = useState(false)
	return (
		<div className='field'>
			<label className='label is-medium'>
				Wir freuen uns auf {plural ? 'Euch' : 'Dich'}.
			</label>
			<div className='control'>
				<button
					className={classNames('button', {
						'is-loading': working,
					})}
					type='button'
					onClick={async () => {
						setWorking(true)
						try {
							await reopen()
						} finally {
							setWorking(false)
						}
					}}
				>
					<span className='icon is-small'>
						<i className='fa-solid fa-rotate-left'></i>
					</span>
				</button>
			</div>
		</div>
	)
}

const Rsvp = ({ party, accept, decline, updateMembers, reopen }: Props) => {
	const { hasAnswered, isAttending, isNotAttending, isFinished } =
		calcStatus(party)

	return (
		<Section title='Teilnahme'>
			<div className='rsvp-section box' data-aos='fade'>
				<New accept={accept} decline={decline} disabled={hasAnswered} />

				{hasAnswered && isNotAttending ? (
					<>
						<hr />
						<Declined accept={accept} />
					</>
				) : null}

				{hasAnswered && isAttending ? (
					<>
						<hr />
						<Accepted
							decline={decline}
							initialMembers={party.members}
							updateMembers={updateMembers}
							disabled={isFinished}
						/>
					</>
				) : null}

				{hasAnswered && isFinished ? (
					<>
						<hr />
						<Finished
							plural={party.members.length > 0}
							reopen={reopen}
						/>
					</>
				) : null}
			</div>
		</Section>
	)
}

export default Rsvp
