import { ComponentStory, ComponentMeta } from '@storybook/react'

import Declined from './Declined'

export default {
	title: 'Views/Invitation/Rsvp/Declined',
	component: Declined,
} as ComponentMeta<typeof Declined>

const Template: ComponentStory<typeof Declined> = (args) => (
	<Declined {...args} />
)

export const Primary = Template.bind({})
Primary.args = {
	accept: () => Promise.resolve(),
}
