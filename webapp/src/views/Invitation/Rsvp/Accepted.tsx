import classNames from 'classnames'
import { useState } from 'react'
import { Member as MemberType, Species, SpeciesList } from '../../../database'
import { generate_id } from '../../../utilities'

type Props = {
	readonly disabled: boolean
	readonly decline: () => Promise<void>
	readonly initialMembers: MemberType[]
	readonly updateMembers: (
		members: MemberType[],
		finish: boolean,
	) => Promise<void>
}
type MemberProps = {
	readonly disabled: boolean
	readonly member: MemberType
	readonly setName: (new_name: string) => void
	readonly setSpecies: (species: Species) => void
	readonly remove: () => void
}

const Member = ({
	member: { name, species },
	setName,
	setSpecies,
	remove,
	disabled,
}: MemberProps) => (
	<div className='field has-addons'>
		<p className='control'>
			<span className='select'>
				<select
					value={species}
					onChange={(e) => setSpecies(e.target.value as Species)}
				>
					{SpeciesList.map((v) => (
						<option key={v} value={v}>
							{v}
						</option>
					))}
				</select>
			</span>
		</p>

		<div className='control'>
			<input
				className='input'
				type='text'
				placeholder='Name'
				value={name}
				onChange={(e) => setName(e.target.value)}
			/>
		</div>
		<div className='control'>
			<button
				className='button'
				type='button'
				disabled={disabled}
				onClick={() => remove()}
			>
				<span className='icon'>
					<i className='fa-solid fa-trash-can' />
				</span>
			</button>
		</div>
	</div>
)

const Accepted = ({
	decline,
	initialMembers,
	updateMembers,
	disabled,
}: Props) => {
	const [working, setWorking] = useState(false)
	const [members, setMembers] = useState<Record<string, MemberType>>(() => {
		let result = {}
		for (const m of initialMembers) {
			// eslint-disable-next-line no-constant-condition
			while (true) {
				const key = generate_id(4)
				if (!(key in result)) {
					result = {
						...result,
						[key]: m,
					}
					break
				}
			}
		}

		return result
	})

	const addMember = (name = '', species: Species = 'Erwachsener') => {
		// eslint-disable-next-line no-constant-condition
		while (true) {
			const key = generate_id(4)
			if (!(key in members)) {
				setMembers((old) => ({
					...old,
					[key]: {
						name,
						species,
					},
				}))
				break
			}
		}
	}
	const removeMember = (id: string) =>
		setMembers((old) => {
			// eslint-disable-next-line @typescript-eslint/no-unused-vars
			const { [id]: _, ...rest } = old
			return rest
		})
	const updateMember = (
		id: string,
		data: { name?: string; species?: Species },
	) =>
		setMembers((old) => ({
			...old,
			[id]: {
				...old[id],
				...data,
			},
		}))

	return (
		<fieldset disabled={disabled}>
			<div className='field'>
				<label className='label is-medium'>Wer begleitet dich?</label>

				{Object.entries(members).map(([key, member]) => (
					<Member
						disabled={disabled}
						member={member}
						key={key}
						remove={() => removeMember(key)}
						setName={(name) => updateMember(key, { name })}
						setSpecies={(species) => updateMember(key, { species })}
					/>
				))}

				<div className='field'>
					<div className='control'>
						<button
							type='button'
							className='button'
							disabled={disabled}
							onClick={() => addMember()}
						>
							<span className='icon'>
								<i className='fa-solid fa-plus' />
							</span>
						</button>
					</div>
				</div>

				<div className='field is-grouped is-grouped-right'>
					<div className='control'>
						<button
							className={classNames('button', 'is-primary', {
								'is-loading': working,
							})}
							type='button'
							onClick={async () => {
								setWorking(true)
								try {
									await updateMembers(
										Object.values(members),
										true,
									)
								} finally {
									setWorking(false)
								}
							}}
						>
							Absenden
						</button>
					</div>
					<div className='control'>
						<button
							className={classNames('button', 'is-light', {
								'is-loading': working,
							})}
							type='button'
							onClick={async () => {
								setWorking(true)
								try {
									await decline()
								} finally {
									setWorking(false)
								}
							}}
						>
							Absagen
						</button>
					</div>
				</div>
			</div>
		</fieldset>
	)
}

export default Accepted
