import { ComponentStory, ComponentMeta } from '@storybook/react'

import New from './New'

export default {
	title: 'Views/Invitation/Rsvp/New',
	component: New,
} as ComponentMeta<typeof New>

const Template: ComponentStory<typeof New> = (args) => <New {...args} />

export const Primary = Template.bind({})
Primary.args = {
	disabled: false,
	accept: () => Promise.resolve(),
	decline: () => Promise.resolve(),
}

export const Disabled = Template.bind({})
Disabled.args = {
	disabled: true,
	accept: () => Promise.resolve(),
	decline: () => Promise.resolve(),
}
