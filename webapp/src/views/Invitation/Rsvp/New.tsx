import classNames from 'classnames'
import { useState } from 'react'
import DateTime from '../../../components/DateTime'
import { begin_date } from '../../../date'

type Props = {
	readonly disabled: boolean
	readonly accept: () => Promise<void>
	readonly decline: () => Promise<void>
}

const New = ({ accept, decline, disabled }: Props) => {
	const [working, setWorking] = useState(false)
	return (
		<fieldset disabled={disabled}>
			<div className='field'>
				<label className='label is-large'>
					{'Kannst du am '}
					{
						<DateTime
							timestamp={begin_date}
							dateStyle='medium'
							timeStyle='short'
						/>
					}
					{' bei unserer Hochzeitsfeier dabei sein?'}
				</label>
				<div className='field is-grouped is-grouped-right'>
					<div className='control'>
						<button
							className={classNames('button', 'is-primary', {
								'is-loading': working,
							})}
							type='button'
							onClick={async () => {
								setWorking(true)
								try {
									await accept()
								} finally {
									setWorking(false)
								}
							}}
						>
							Klar
						</button>
					</div>
					<div className='control'>
						<button
							className={classNames('button', 'is-light', {
								'is-loading': working,
							})}
							type='button'
							onClick={async () => {
								setWorking(true)
								try {
									await decline()
								} finally {
									setWorking(false)
								}
							}}
						>
							Leider nicht
						</button>
					</div>
				</div>
			</div>
		</fieldset>
	)
}

export default New
