import classNames from 'classnames'
import { useState } from 'react'

type Props = {
	readonly accept: () => Promise<void>
}

const Declined = ({ accept }: Props) => {
	const [working, setWorking] = useState(false)
	return (
		<div className='field'>
			<label className='label is-medium'>
				Du hast mitgeteilt, dass du leider nicht dabei sein kannst.
			</label>
			<div className='field is-grouped is-grouped-right'>
				<div className='control'>
					<button
						className={classNames('button', 'is-primary', {
							'is-loading': working,
						})}
						type='button'
						onClick={async () => {
							setWorking(true)
							try {
								await accept()
							} finally {
								setWorking(false)
							}
						}}
					>
						Ich moechte doch teilnehmen
					</button>
				</div>
			</div>
		</div>
	)
}

export default Declined
