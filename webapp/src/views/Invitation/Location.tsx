import DateTime from '../../components/DateTime'
import Section from '../../components/Section'
import { begin_date, ceremony_date } from '../../date'
import './Location.css'

const Location = () => (
	<>
		<div className='location-title'>
			<section className='hero is-large'>
				<div className='hero-body'>
					<div className='container has-text-centered'>
						<h1 className='title'>Cafe Lindauhof</h1>
					</div>
				</div>
			</section>
		</div>
		<Section title='Location und Uhrzeit'>
			<div className='columns is-multiline is-centered'>
				<div className='column is-half location' data-aos='fade'>
					<p className='title'>Cafe Lindauhof</p>
					<br />
					<p className='content is-medium'>
						<p>
							<span className='tag is-large'>Adresse</span>
							Lindauhof 4, 24392 Boren
						</p>
						<p>
							<span className='tag is-large'>Uhrzeit</span>
							<DateTime
								timestamp={begin_date}
								timeStyle='short'
							/>
						</p>
						<p>
							<span className='tag is-large'>Trauung</span>
							<DateTime
								timestamp={ceremony_date}
								timeStyle='short'
							/>
						</p>
					</p>
				</div>
				<div className='column is-half' data-aos='fade'>
					<figure className='image is-4by3'>
						<iframe
							className='has-ratio'
							src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d18495.06293567459!2d9.790140941781441!3d54.58843800000002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47b3106afb6e0901%3A0xffa5d7a798b9c59f!2sCaf%C3%A9%20Lindauhof%20-%20Landarzthaus!5e0!3m2!1sen!2sde!4v1655657054868!5m2!1sen!2sde'
							width='800'
							height='600'
							allowFullScreen={false}
							loading='lazy'
							frameBorder={0}
							referrerPolicy='no-referrer-when-downgrade'
						/>
					</figure>
				</div>
			</div>
		</Section>
	</>
)

export default Location
