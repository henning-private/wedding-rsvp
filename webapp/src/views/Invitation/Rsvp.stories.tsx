import { ComponentStory, ComponentMeta } from '@storybook/react'

import Rsvp from './Rsvp'
import { Member, Party as PartyType } from '../../database'
import { useState } from 'react'

export default {
	title: 'Views/Invitation/Rsvp',
	component: Rsvp,
} as ComponentMeta<typeof Rsvp>

const Template: ComponentStory<typeof Rsvp> = (args) => <Rsvp {...args} />

export const Start = Template.bind({})
Start.args = {
	party: {
		id: '0815',
		members: [],
		last_seen: new Date('Jun 8, 2022, 12:19 PM'),
		name: 'Hello World',
		greeting: 'Hallo',
	},
	accept: () => Promise.resolve(),
	decline: () => Promise.resolve(),
	updateMembers: () => Promise.resolve(),
}

export const Accepted = Template.bind({})
Accepted.args = {
	party: {
		id: '0815',
		members: [],
		attending: true,
		last_seen: new Date('Jun 8, 2022, 12:19 PM'),
		name: 'Hello World',
		greeting: 'Hallo',
	},
	accept: () => Promise.resolve(),
	decline: () => Promise.resolve(),
	updateMembers: () => Promise.resolve(),
}

export const Declined = Template.bind({})
Declined.args = {
	party: {
		id: '0815',
		members: [],
		attending: false,
		last_seen: new Date('Jun 8, 2022, 12:19 PM'),
		name: 'Hello World',
		greeting: 'Hallo',
	},
	accept: () => Promise.resolve(),
	decline: () => Promise.resolve(),
	updateMembers: () => Promise.resolve(),
}

export const Finished_With_Members = Template.bind({})
Finished_With_Members.args = {
	party: {
		id: '0815',
		members: [{ name: 'Struppi', species: 'Hund' }],
		attending: true,
		finished: true,
		last_seen: new Date('Jun 8, 2022, 12:19 PM'),
		name: 'Hello World',
		greeting: 'Hallo',
	},
	accept: () => Promise.resolve(),
	decline: () => Promise.resolve(),
	updateMembers: () => Promise.resolve(),
}

export const Finished_Without_Members = Template.bind({})
Finished_Without_Members.args = {
	party: {
		id: '0815',
		members: [],
		attending: true,
		finished: true,
		last_seen: new Date('Jun 8, 2022, 12:19 PM'),
		name: 'Hello World',
		greeting: 'Hallo',
	},
	accept: () => Promise.resolve(),
	decline: () => Promise.resolve(),
	updateMembers: () => Promise.resolve(),
}

export const delay = (ms: number) =>
	new Promise((resolve) => setTimeout(resolve, ms))

export const Interactive = () => {
	const [party, setParty] = useState<PartyType>({
		id: '0815',
		members: [],
		last_seen: new Date('Jun 8, 2022, 12:19 PM'),
		name: 'Hello World',
		greeting: 'Hallo',
	})

	const accept = async () => {
		await delay(500)
		setParty((old) => ({ ...old, attending: true }))
	}
	const decline = async () => {
		await delay(500)
		setParty((old) => ({ ...old, attending: false }))
	}
	const reopen = async () => {
		await delay(500)
		setParty((old) => ({ ...old, finished: false }))
	}
	const updateMembers = async (members: Member[], finish: boolean) => {
		await delay(500)
		setParty((old) => ({
			...old,
			members: members.filter((m) => m.name?.length),
			finished: finish,
		}))
	}

	return (
		<Rsvp
			accept={accept}
			decline={decline}
			party={party}
			updateMembers={updateMembers}
			reopen={reopen}
		/>
	)
}
