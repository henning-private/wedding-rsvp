import { ComponentStory, ComponentMeta } from '@storybook/react'

import Accommodation from './Accommodation'

export default {
	title: 'Views/Invitation/Accommodation',
	component: Accommodation,
} as ComponentMeta<typeof Accommodation>

const Template: ComponentStory<typeof Accommodation> = () => <Accommodation />

export const Primary = Template.bind({})
