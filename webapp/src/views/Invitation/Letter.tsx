import { Party as PartyType } from '../../database'
import Section from '../../components/Section'
import ImageSlider from '../../components/ImageSlider'
import img1 from './nora_henning_zugspitze.jpg'
import img2 from './kiss_paris.jpg'
import img3 from './nora_henning_island.jpg'
import './Letter.css'
import { ceremony_date } from '../../date'
import DateTime from '../../components/DateTime'

type Props = {
	readonly party: PartyType
}

const Letter = ({ party: { greeting } }: Props) => {
	return (
		<Section title='Einladung'>
			<div className='columns is-multiline is-centered is-vcentered'>
				<div className='column is-half' data-aos='fade'>
					<p className='title is-3'>
						<strong>{greeting}</strong>
					</p>
					<br />
					<p className='content is-medium'>
						<p>Wir sagen JA!</p>
						<blockquote>
							Freude lässt sich nur voll auskosten, wenn sich ein
							anderer mitfreut.
							<p className='citation-author'>Mark Twain</p>
						</blockquote>
						<p>
							Daher möchten wir diesen besonderen Moment gerne mit
							Euch teilen und laden Euch herzlich zu unserer
							Hochzeitsfeier ein.
							<br />
							{'Unsere freie Trauung findet am '}
							<DateTime
								timestamp={ceremony_date}
								dateStyle='long'
							/>
							{' statt.'}
						</p>
						<p>Wir freuen uns auf Euch!</p>
					</p>
				</div>
				<div className='column is-half' data-aos='fade'>
					<div className='letter-image'>
						<ImageSlider
							images={[img1, img2, img3]}
							interval={10000}
						/>
					</div>
				</div>
			</div>
		</Section>
	)
}

export default Letter
