import { useEffect, useState } from 'react'
import { useParams, useNavigate } from 'react-router-dom'
import Loader from '../../components/Loader'
import {
	get_party,
	Party as PartyType,
	update_last_seen,
	accept_invitation,
	decline_invitation,
	update_members,
	reopen as db_reopen,
	Member as MemberType,
} from '../../database'
import Letter from './Letter'
import Rsvp from './Rsvp'
import FAQ from './FAQ'
import Location from './Location'
import Accommodation from './Accommodation'

const Invitation = () => {
	const navigate = useNavigate()
	const partyId = useParams().partyId?.toLowerCase() ?? ''
	const [party, setParty] = useState<PartyType | undefined>()

	const fetchParty = async () => {
		const party = await get_party(partyId)
		if (!party) navigate('/')

		if (Date.now() - (party?.last_seen?.valueOf() ?? 0) > 60 * 1000) {
			try {
				await update_last_seen(partyId)
			} catch {
				/* NOTHING */
			}
		}
		setParty(party)
	}

	const accept = async () => {
		if (!party) return

		await accept_invitation(party.id)
		await fetchParty()
	}
	const decline = async () => {
		if (!party) return

		await decline_invitation(party.id)
		await fetchParty()
	}
	const reopen = async () => {
		if (!party) return

		await db_reopen(party.id)
		await fetchParty()
	}
	const updateMembers = async (members: MemberType[], finish: boolean) => {
		if (!party) return

		await update_members(party.id, members, finish)
		await fetchParty()
	}

	useEffect(() => {
		fetchParty()
	}, [partyId])

	if (!party) return <Loader />

	return (
		<>
			<div className='box'>
				<nav className='level is-size-4'>
					<p className='level-item has-text-centered'>
						<a href='#invitation'>Einladung</a>
					</p>
					<p className='level-item has-text-centered'>
						<a href='#rsvp'>Teilnahme</a>
					</p>
					<p className='level-item has-text-centered'>
						<a href='#location'>Location</a>
					</p>
					<p className='level-item has-text-centered'>
						<a href='#accomodation'>Unterkunft</a>
					</p>
					<p className='level-item has-text-centered'>
						<a href='#faq'>FAQ</a>
					</p>
				</nav>
			</div>

			<div id='invitation'>
				<Letter party={party} />
			</div>
			<div id='rsvp'>
				<Rsvp
					party={party}
					accept={accept}
					decline={decline}
					updateMembers={updateMembers}
					reopen={reopen}
				/>
			</div>
			<div id='location'>
				<Location />
			</div>
			<div id='accomodation'>
				<Accommodation />
			</div>
			<div id='faq'>
				<FAQ />
			</div>
		</>
	)
}

export default Invitation
