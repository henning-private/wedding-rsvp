import { ReactNode } from 'react'
import DateTime from '../../components/DateTime'
import Section from '../../components/Section'
import { polterabend_date } from '../../date'

const faq = [
	{
		q: 'Gibt es eine Kleiderordnung?',
		a: (
			<>
				Festliche Garderobe
				<br />
				<strong>Hinweis:</strong> Allen Ladies empfehlen wir zusätzlich
				flache Schuhe mitzubringen (Kopfsteinpflaster :-))
			</>
		),
	},
	{
		q: 'Was wünscht ihr euch?',
		a: (
			<p>
				Unser Heim ist klein,
				<br />
				es passt auch nichts mehr rein.
				<br />
				Drum schenkt uns keine schönen Sachen,
				<br />
				lasst lieber unseren Sparstrumpf lachen.
			</p>
		),
	},
	{
		q: 'Gibt es einen Polterabend?',
		a: (
			<p>
				{'Ja, am '}
				<DateTime
					timestamp={polterabend_date}
					dateStyle={'short'}
					timeStyle={'short'}
				/>{' '}
				in Winnemark, Bockholz 2-4.
				<br />
				Meldet Euch gerne bei Hanna (
				<a href='tel:+4915753067644'>+49 1575 3067644</a>
				), wenn Ihr Lust und Zeit habt dabei zu sein.
			</p>
		),
	},
]

const Card = ({ title, content }: { title: string; content: ReactNode }) => (
	<div className='card block' data-aos='fade'>
		<header className='card-header'>
			<p className='card-header-title'>{title}</p>
		</header>
		<div className='card-content'>
			<div className='content'>{content}</div>
		</div>
	</div>
)

const FAQ = () => (
	<Section title='FAQ'>
		{faq.map((x) => (
			<Card key={x.q} title={x.q} content={x.a} />
		))}
	</Section>
)

export default FAQ
