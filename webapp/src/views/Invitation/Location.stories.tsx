import { ComponentStory, ComponentMeta } from '@storybook/react'

import Location from './Location'

export default {
	title: 'Views/Invitation/Location',
	component: Location,
} as ComponentMeta<typeof Location>

const Template: ComponentStory<typeof Location> = () => <Location />

export const Primary = Template.bind({})
