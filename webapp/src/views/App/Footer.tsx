import { Link } from 'react-router-dom'

const Footer = () => (
	<footer className='footer'>
		<nav className='level'>
			<p className='level-item has-text-centered'>
				<a
					className='link is-info'
					href='https://gitlab.com/henning-private/wedding-rsvp'
				>
					Source
				</a>
			</p>
			<div className='level-item has-text-centered'>
				<div>
					<p className='heading'>© 2022 Henning Vespermann</p>
				</div>
			</div>
			<p className='level-item has-text-centered'>
				<Link className='link is-info' to='/admin'>
					Admin
				</Link>
			</p>
		</nav>
	</footer>
)

export default Footer
