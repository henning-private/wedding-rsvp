import { Link } from 'react-router-dom'

const NotFound = () => (
	<section className='section'>
		<h1 className='title'>404</h1>
		<h2 className='subtitle'>Nothing to see here!</h2>
		<p>
			<Link to='/'>Go to the home page</Link>
		</p>
	</section>
)

export default NotFound
