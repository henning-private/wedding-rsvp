import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Admin from '../Admin'
import Invitation from '../Invitation'
import Layout from './Layout'
import NotFound from './NotFound'

const App = () => (
	<BrowserRouter>
		<Routes>
			<Route path='/' element={<Layout />}>
				<Route path=':partyId' element={<Invitation />} />
				<Route path='admin' element={<Admin />} />
				<Route path='*' element={<NotFound />} />
			</Route>
		</Routes>
	</BrowserRouter>
)

export default App
