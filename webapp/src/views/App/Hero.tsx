import DateTime from '../../components/DateTime'
import { ceremony_date } from '../../date'
import './Hero.css'

const Hero = () => (
	<div className='main-title'>
		<section className='hero is-fullheight'>
			<div className='hero-body'>
				<div className='container has-text-centered'>
					<h2 className='title'>Nora &amp; Henning</h2>
					<br />
					<h1 className='subtitle'>
						<DateTime timestamp={ceremony_date} dateStyle='long' />
					</h1>
				</div>
			</div>
		</section>
	</div>
)

export default Hero
