import { Outlet } from "react-router-dom"
import Footer from "./Footer"
import Hero from "./Hero"

const Layout = () => (
	<div>
		<Hero />
		<Outlet />
		<Footer />
	</div>
)
export default Layout
