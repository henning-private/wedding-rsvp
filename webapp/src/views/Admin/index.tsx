import { useEffect, useState } from 'react'
import { auth, signIn } from '../../firebase'
import {
	Party as PartyType,
	get_parties,
	new_party,
	update_party,
	delete_party,
} from '../../database'
import PartyList from './PartyList'
import { usePartyDialog } from './PartyDialog'

const Admin = () => {
	const [user, setUser] = useState(auth.currentUser)
	const [parties, setParties] = useState<PartyType[]>([])

	useEffect(() => auth.onAuthStateChanged((user) => setUser(user)), [])

	const fetchParties = async () => setParties(await get_parties())
	useEffect(() => {
		if (user) fetchParties()
	}, [user])

	const { Dialog, show } = usePartyDialog({
		title: 'Create new party',
		onSubmit: async (name, greeting) => {
			const p = await new_party({
				name,
				greeting,
				members: [],
			})
			setParties((old) => old.concat(p))
		},
	})
	const showNew = () => show('', '')

	return (
		<>
			{Dialog}
			<section className='section'>
				<h1 className='title'>Admin</h1>
				<h2 className='subtitle'>Manage attendance</h2>
				{user ? (
					<>
						<PartyList
							parties={parties}
							update={async (id, name, greeting) => {
								await update_party(id, name, greeting)
								setParties((old) => {
									const idx = old.findIndex(
										(p) => p.id === id,
									)
									return [
										...old.slice(0, idx),
										{
											...old[idx],
											name,
											greeting,
										},
										...old.slice(idx + 1),
									]
								})
							}}
							remove={async (id) => {
								await delete_party(id)
								setParties((old) =>
									old.filter((p) => p.id !== id),
								)
							}}
						/>
						<div className='field is-grouped'>
							<div className='control'>
								<button
									className='button'
									type='button'
									onClick={showNew}
								>
									new
								</button>
							</div>
							<div className='control'>
								<button
									className='button'
									type='button'
									onClick={() => auth.signOut()}
								>
									Logout
								</button>
							</div>
							<div className='control'>
								<button
									className='button'
									type='button'
									onClick={fetchParties}
								>
									Refresh
								</button>
							</div>
						</div>
					</>
				) : (
					<button className='button' type='button' onClick={signIn}>
						Login
					</button>
				)}
			</section>
		</>
	)
}

export default Admin
