import classNames from 'classnames'
import { useState } from 'react'
import Dialog from '../../components/Dialog'
import { useTextInput } from '../../components/TextInput'

export const usePartyForm = () => {
	const validate = (val: string | undefined) => !!val?.length

	const [name, setName, NameInput] = useTextInput({
		label: 'Name',
		placeholder: 'Name',
		validate,
		icon: <i className='fa-solid fa-user' />,
	})
	const [greeting, setGreeting, GreetingInput] = useTextInput({
		label: 'Greeting',
		placeholder: 'Greeting',
		validate,
		icon: <i className='fa-solid fa-comment-dots' />,
	})

	const form = (
		<>
			{NameInput}
			{GreetingInput}
		</>
	)

	return {
		name,
		setName,
		greeting,
		setGreeting,
		isValid: validate(name) && validate(greeting),
		form,
	}
}

type Props = {
	title: string
	onSubmit: (name: string, greeting: string) => Promise<void>
}

export const usePartyDialog = ({ title, onSubmit }: Props) => {
	const [active, setActive] = useState(false)
	const hide = () => setActive(false)
	const [submitting, setSubmitting] = useState(false)

	const { name, setName, greeting, setGreeting, isValid, form } =
		usePartyForm()
	const show = (name: string, greeting: string) => {
		setActive(true)
		setName(name)
		setGreeting(greeting)
	}
	const reset = () => {
		setName('')
		setGreeting('')
	}

	const submit = async () => {
		if (isValid) {
			setSubmitting(true)
			try {
				await onSubmit(name, greeting)
				reset()
				hide()
			} finally {
				setSubmitting(false)
			}
		}
	}

	const cancel = async () => {
		hide()
	}

	const footer = (
		<>
			<button
				type='button'
				className={classNames('button', 'is-primary', {
					'is-loading': submitting,
				})}
				onClick={submit}
				disabled={!isValid}
			>
				OK
			</button>
			<button
				type='button'
				className='button'
				onClick={cancel}
				disabled={submitting}
			>
				Cancel
			</button>
		</>
	)

	return {
		show,
		hide,
		Dialog: (
			<Dialog
				isActive={active}
				title={title}
				footer={footer}
				onClose={hide}
			>
				{form}
			</Dialog>
		),
	}
}
