import { ComponentStory, ComponentMeta } from '@storybook/react'

import Party from './Party'

export default {
	title: 'Views/Admin/Party',
	component: Party,
	decorators: [
		(Story) => (
			<table className='table'>
				<tbody>
					<Story />
				</tbody>
			</table>
		),
	],
	argTypes: {
		party: {
			control: { type: 'object' },
		},
	},
} as ComponentMeta<typeof Party>

const Template: ComponentStory<typeof Party> = (args) => <Party {...args} />

export const Default = Template.bind({})
Default.args = {
	party: {
		id: '0815',
		members: [],
		last_seen: new Date('Jun 8, 2022, 12:19 PM'),
		name: 'Hello World',
		greeting: 'Hallo',
	},
}

export const WithoutTimeStamp = Template.bind({})
WithoutTimeStamp.args = {
	party: {
		id: 'bh54',
		members: [],
		name: 'No Timestamp',
		greeting: 'Hallo',
	},
}

export const WithMembers = Template.bind({})
WithMembers.args = {
	party: {
		id: 'helo',
		members: [
			{ name: 'Horst', species: 'Erwachsener' },
			{ name: 'Gundula', species: 'Erwachsener' },
			{ name: 'Klein Erna', species: 'Kind' },
			{ name: 'Bello', species: 'Hund' },
		],
		last_seen: new Date('Jun 5, 2022, 8:23 PM'),
		last_update: new Date('Jun 8, 2022, 12:19 PM'),
		name: 'Horst',
		greeting: 'Hallo',
	},
}

export const UnknownStatus = Template.bind({})
UnknownStatus.args = {
	party: {
		id: 'bh54',
		members: [],
		name: 'Unknown Status',
		greeting: 'Hallo',
	},
}

export const InProgressStatus = Template.bind({})
InProgressStatus.args = {
	party: {
		id: 'bh54',
		members: [],
		name: 'In Progress Status',
		greeting: 'Hallo',
		last_seen: new Date('Jun 5, 2022, 8:23 PM'),
	},
}

export const DeclinedStatus = Template.bind({})
DeclinedStatus.args = {
	party: {
		id: 'bh54',
		members: [],
		name: 'Declined Status',
		greeting: 'Hallo',
		last_seen: new Date('Jun 5, 2022, 8:23 PM'),
		attending: false,
	},
}

export const AttendingInProgressStatus = Template.bind({})
AttendingInProgressStatus.args = {
	party: {
		id: 'bh54',
		members: [],
		name: 'Attending In Progress Status',
		greeting: 'Hallo',
		last_seen: new Date('Jun 5, 2022, 8:23 PM'),
		attending: true,
	},
}

export const AttendingStatus = Template.bind({})
AttendingStatus.args = {
	party: {
		id: 'bh54',
		members: [],
		name: 'Attending In Progress Status',
		greeting: 'Hallo',
		last_seen: new Date('Jun 5, 2022, 8:23 PM'),
		attending: true,
		finished: true,
	},
}
