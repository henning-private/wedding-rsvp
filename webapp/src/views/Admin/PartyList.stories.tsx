import { ComponentStory, ComponentMeta } from '@storybook/react'

import PartyList from './PartyList'

export default {
	title: 'Views/Admin/PartyList',
	component: PartyList,
} as ComponentMeta<typeof PartyList>

const Template: ComponentStory<typeof PartyList> = (args) => (
	<PartyList {...args} />
)

export const Default = Template.bind({})
Default.args = {
	parties: [
		{
			id: '0815',
			members: [],
			last_seen: new Date('Jun 8, 2022, 12:19 PM'),
			name: 'Hello World',
			greeting: 'Hallo',
		},
		{
			id: 'bh54',
			members: [],
			name: 'No Timestamp',
			greeting: 'Hallo',
		},
		{
			id: 'helo',
			members: [
				{ name: 'Horst', species: 'Erwachsener' },
				{ name: 'Gundula', species: 'Erwachsener' },
				{ name: 'Klein Erna', species: 'Kind' },
				{ name: 'Bello', species: 'Hund' },
			],
			last_seen: new Date('Jun 5, 2022, 8:23 PM'),
			name: 'Fubar',
			greeting: 'Hallo',
		},
	],
}
