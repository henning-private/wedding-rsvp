import { Party as PartyType } from '../../database'
import Party from './Party'

type PartyListProps = {
	readonly parties: PartyType[]
	readonly update: (
		id: string,
		name: string,
		greeting: string,
	) => Promise<void>
	readonly remove: (id: string) => Promise<void>
}

const PartyList = ({ parties, update, remove }: PartyListProps) => (
	<table className='table'>
		<thead>
			<tr>
				<th>ID</th>
				<th>Last Seen</th>
				<th>Last Update</th>
				<th>Name</th>
				<th>Greeting</th>
				<th>Members</th>
				<th>Status</th>
				<th>Url</th>
				<th>Edit</th>
				<th>Delete</th>
			</tr>
		</thead>
		<tbody>
			{parties.map((p) => (
				<Party
					party={p}
					key={p.id}
					update={(name, greeting) => update(p.id, name, greeting)}
					remove={() => remove(p.id)}
				/>
			))}
		</tbody>
	</table>
)

export default PartyList
