import { render, screen } from '@testing-library/react'
import Party from './Party'
import { Party as PartyType } from '../../database'

test('Table Row displays all provided values', () => {
	const party: PartyType = {
		id: '0815',
		members: [],
		last_seen: new Date('Jun 5, 2022, 8:23 PM'),
		last_update: new Date('Jun 8, 2022, 12:19 PM'),
		name: 'Hello World',
		greeting: 'Hallo',
	}

	render(
		<table>
			<tbody>
				<Party
					party={party}
					remove={() => Promise.resolve()}
					update={() => Promise.resolve()}
				/>
			</tbody>
		</table>,
	)

	expect(screen.getByText(party.id)).toBeDefined()
	expect(screen.getByText(party.name)).toBeDefined()
	expect(screen.getByText(party.greeting)).toBeDefined()
	expect(
		screen.getByText((c) => Date.parse(c) === party.last_seen?.getTime()),
	).toBeDefined()
	expect(
		screen.getByText((c) => Date.parse(c) === party.last_update?.getTime()),
	).toBeDefined()
})
