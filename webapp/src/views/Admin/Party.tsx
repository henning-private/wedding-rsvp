import classNames from 'classnames'
import DateTime from '../../components/DateTime'
import { Party as PartyType } from '../../database'
import { usePartyDialog } from './PartyDialog'

type PartyProps = {
	readonly party: PartyType
	readonly update: (name: string, greeting: string) => Promise<void>
	readonly remove: () => Promise<void>
}

const Party = ({
	party: {
		id,
		last_seen,
		last_update,
		name,
		greeting,
		members,
		attending,
		finished,
	},
	update,
	remove,
}: PartyProps) => {
	const { show, Dialog } = usePartyDialog({
		title: 'Edit party',
		onSubmit: async (n, g) => {
			if (n != name || g != greeting) await update(n, g)
		},
	})

	const link = `${location.origin}/${id}`

	const mem = members.map((m) => (
		<p key={m.name}>
			<span className='icon-text'>
				<span className='icon'>
					<i
						className={classNames('fa-solid', {
							'fa-dog': m.species === 'Hund',
							'fa-user': m.species === 'Erwachsener',
							'fa-child': m.species === 'Kind',
						})}
					/>
				</span>
				<span>{m.name}</span>
			</span>
		</p>
	))

	return (
		<>
			{Dialog}
			<tr>
				<td>{id}</td>
				<td>
					{last_seen ? (
						<DateTime
							timestamp={last_seen}
							dateStyle='short'
							timeStyle='short'
						/>
					) : (
						'Never'
					)}
				</td>
				<td>
					{last_update ? (
						<DateTime
							timestamp={last_update}
							dateStyle='short'
							timeStyle='short'
						/>
					) : (
						'Never'
					)}
				</td>
				<td>{name}</td>
				<td>{greeting}</td>
				<td>{mem}</td>
				<td>
					<span className='icon'>
						<i
							className={classNames('fa-solid', {
								'fa-spinner':
									last_seen &&
									!finished &&
									attending == undefined,
								'fa-check-double': attending && finished,
								'fa-check': attending && !finished,
								'fa-xmark': attending === false,
								'fa-question': !last_seen,
							})}
						/>
					</span>
				</td>
				<td>
					<a href={link}>{link}</a>
				</td>
				<td>
					<button
						type='button'
						className='button'
						onClick={() => show(name, greeting)}
					>
						<span className='icon is-small'>
							<i className='fa-solid fa-pencil'></i>
						</span>
					</button>
				</td>
				<td>
					<button type='button' className='button' onClick={remove}>
						<span className='icon is-small'>
							<i className='fa-solid fa-trash-can' />
						</span>
					</button>
				</td>
			</tr>
		</>
	)
}

export default Party
