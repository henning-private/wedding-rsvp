
const alphabet = 'abcdefghijklmnopqrstuvwxyz1234567890'

export const generate_id = (length: number): string => {
	let id = ''
	for (let i = 0; i < length; i++)
		id += alphabet.charAt(Math.floor(Math.random() * alphabet.length))
	return id
}
