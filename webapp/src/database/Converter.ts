import {
	QueryDocumentSnapshot,
	DocumentData,
	SnapshotOptions,
	FirestoreDataConverter,
	WithFieldValue,
	Timestamp,
} from 'firebase/firestore'
import { Party } from './types'

class Converter implements FirestoreDataConverter<Party> {
	fromFirestore(
		snapshot: QueryDocumentSnapshot<DocumentData>,
		options?: SnapshotOptions,
	): Party {
		return {
			id: snapshot.id,
			last_seen: (<Timestamp>(
				snapshot.get('last_seen', options)
			))?.toDate(),
			last_update: (<Timestamp>(
				snapshot.get('last_update', options)
			))?.toDate(),
			attending: snapshot.get('attending', options),
			finished: snapshot.get('finished', options),
			greeting: snapshot.get('greeting', options),
			deleted: snapshot.get('deleted', options),
			name: snapshot.get('name', options),
			members: snapshot.get('members', options),
		}
	}
	toFirestore({
		last_seen,
		last_update,
		attending,
		finished,
		deleted,
		greeting,
		members,
		name,
	}: WithFieldValue<Party>): DocumentData {
		return {
			last_seen: last_seen ? Timestamp.fromDate(<Date>last_seen) : null,
			last_update: last_update
				? Timestamp.fromDate(<Date>last_update)
				: null,
			attending: attending ?? null,
			finished: finished ?? null,
			deleted: deleted ?? null,
			greeting,
			members,
			name,
		}
	}
}

const converter = new Converter()

export default converter
