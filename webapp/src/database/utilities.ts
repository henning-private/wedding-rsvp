import { db } from '../firebase'
import {
	collection,
	setDoc,
	doc,
	serverTimestamp,
	getDoc,
	getDocFromServer,
	getDocs,
	updateDoc,
} from 'firebase/firestore'
import converter from './Converter'
import { Member, Party } from './types'
import { generate_id } from '../utilities'

const partiesCollection = () =>
	collection(db, 'parties').withConverter(converter)
const partyDoc = (id: string) => doc(partiesCollection(), id)

export const new_party = async (party: Omit<Party, 'id'>): Promise<Party> => {
	const partiesCol = partiesCollection()

	// eslint-disable-next-line no-constant-condition
	while (true) {
		const id = generate_id(4)
		const docRef = doc(partiesCol, id)
		const d = await getDocFromServer(docRef)
		if (!d.exists()) {
			await setDoc(docRef, party)
			return {
				...party,
				id,
			}
		}
	}
}

export const delete_party = async (id: string) => {
	const partiesCol = partiesCollection()
	// await deleteDoc(doc(partiesCol, id))
	await updateDoc(doc(partiesCol, id), {
		last_update: serverTimestamp(),
		deleted: true
	})
}

export const update_party = async (id: string, name: string, greeting: string) => {
	const partiesCol = partiesCollection()
	await updateDoc(doc(partiesCol, id), {
		last_update: serverTimestamp(),
		name,
		greeting
	})
}

export const update_last_seen = async (id: string) => {
	const partiesCol = partiesCollection()
	await updateDoc(doc(partiesCol, id), { last_seen: serverTimestamp() })
}

export const get_party = async (id: string): Promise<Party | undefined> => {
	const party = await getDoc(partyDoc(id))
	return party.exists() && !party.get('deleted') ? party.data() : undefined
}

export const get_parties = async (): Promise<Party[]> => {
	const partiesCol = partiesCollection()
	const docs = await getDocs(partiesCol)
	return docs.docs.filter(p => !p.get('deleted')).map(p => p.data())
}

export const update_members = async (id: string, members: Member[], finish = false) => {
	await updateDoc(partyDoc(id), {
		members: members.filter(m => m.name?.length),
		finished: finish,
		last_update: serverTimestamp(),
	})
}

export const accept_invitation = async (id: string) => {
	const doc = partyDoc(id)
	await updateDoc(doc, {
		attending: true,
		last_update: serverTimestamp(),
	})
}

export const reopen = async (id: string) => {
	const doc = partyDoc(id)
	await updateDoc(doc, {
		finished: false,
		last_update: serverTimestamp(),
	})
}

export const decline_invitation = async (id: string) => {
	await updateDoc(partyDoc(id), {
		attending: false,
		last_update: serverTimestamp(),
	})
}
