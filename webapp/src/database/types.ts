
export const SpeciesList = ['Erwachsener', 'Kind', 'Hund'] as const
export type Species = typeof SpeciesList[number]

export type Member = {
	readonly name: string
	readonly species: Species
}

export type Party = {
	id: string
	name: string
	greeting: string
	attending?: boolean
	finished?: boolean
	deleted?: boolean
	members: Member[]
	last_seen?: Date
	last_update?: Date
}
