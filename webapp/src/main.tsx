import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './views/App'

import 'bulma/css/bulma.min.css'

import '@fortawesome/fontawesome-free/css/fontawesome.min.css'
import '@fortawesome/fontawesome-free/css/solid.min.css'

import AOS from 'aos'
import 'aos/dist/aos.css'

AOS.init()

// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
ReactDOM.createRoot(document.getElementById('root')!).render(
	<React.StrictMode>
		<App />
	</React.StrictMode>,
)
