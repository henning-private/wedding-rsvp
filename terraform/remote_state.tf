terraform {
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/36491661/terraform/state/terraform_state"
    lock_address   = "https://gitlab.com/api/v4/projects/36491661/terraform/state/terraform_state/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/36491661/terraform/state/terraform_state/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }
}
