variable "domain_name" {
  type = string
}

variable "ipv4_records" {
  type = list(string)
}

variable "txt_records" {
  type = list(string)
}

variable "tags" {
  type    = map(string)
  default = {}
}
