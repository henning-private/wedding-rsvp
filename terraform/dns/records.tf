resource "aws_route53_record" "apex_a" {
  zone_id = aws_route53_zone.primary.zone_id
  name    = aws_route53_zone.primary.name
  type    = "A"
  ttl     = "300"
  records = var.ipv4_records
}

resource "aws_route53_record" "apex_txt" {
  count   = length(var.txt_records) > 0 ? 1 : 0

  zone_id = aws_route53_zone.primary.zone_id
  name    = aws_route53_zone.primary.name
  type    = "TXT"
  ttl     = "300"
  records = var.txt_records
}

resource "aws_route53_record" "www_redirect" {
  zone_id = aws_route53_zone.primary.zone_id
  name    = "www.${aws_route53_record.apex_a.name}"
  type    = "A"
  alias {
    name                   = aws_route53_record.apex_a.name
    zone_id                = aws_route53_zone.primary.zone_id
    evaluate_target_health = true
  }
}
