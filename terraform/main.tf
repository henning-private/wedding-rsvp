locals {
  firebase_ips = ["199.36.158.100"]
  domains = [
    {
      name = "nora-und-henning.de"
      txt  = ["google-site-verification=ZKY-YNKSJExoIA87UshMmn2NPR537xvzQ-efmi8RpXk"]
    }
  ]
}

module "dns_entries" {
  source   = "./dns"
  for_each = { for d in local.domains : d.name => d }

  domain_name  = each.key
  ipv4_records = local.firebase_ips
  txt_records  = each.value.txt
  tags = {
    "project_group" = "wedding nora und henning"
  }
}
