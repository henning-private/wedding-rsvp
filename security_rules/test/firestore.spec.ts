import { readFileSync } from 'fs';

import {
	initializeTestEnvironment,
	assertFails,
	assertSucceeds,
	RulesTestEnvironment,

} from '@firebase/rules-unit-testing';

import {
	doc,
	getDoc,
	setDoc,
	serverTimestamp,
	setLogLevel,
	collection,
	Timestamp,
	getDocs,
	deleteDoc,
	updateDoc,
} from 'firebase/firestore';
import { it } from 'mocha';

export const delay = ( ms: number ): Promise<void> => new Promise( resolve => setTimeout( resolve, ms ) )

let testEnv: RulesTestEnvironment;

before(async () => {
	setLogLevel('error');

	testEnv = await initializeTestEnvironment({
		firestore: { rules: readFileSync('firestore.rules', 'utf8') },
	});
});

after(async () => {
	await testEnv.cleanup();
});

beforeEach(async () => {
	await testEnv.clearFirestore();

	// Add some Parties to DB
	await testEnv.withSecurityRulesDisabled(async (ctx) => {
		const parties = collection(ctx.firestore(), 'parties')
		await setDoc(doc(parties, 'NEW'), {
			name: 'New Dataset',
			greeting: 'Hello',
			members: []
		})
		await setDoc(doc(parties, 'SEEN'), {
			name: 'Seen Dataset',
			greeting: 'Hi, I habe been seen',
			last_seen: Timestamp.fromDate(new Date('Jun 8, 2022, 12:19 PM')),
			members: []
		})
		await setDoc(doc(parties, 'ACCEPTED'), {
			members: [
				{ name: 'Horst' },
				{ name: 'Gundula' },
				{
					name: 'Klein Erna',
					age: 5,
				},
			],
			attending: true,
			last_seen: Timestamp.fromDate(new Date('Jun 5, 2022, 8:23 PM')),
			last_update: Timestamp.fromDate(new Date('Jun 8, 2022, 12:19 PM')),
			name: 'Horst',
			greeting: 'Hallo',
		})
	})

	// Add some Users to DB
	await testEnv.withSecurityRulesDisabled(async (ctx) => {
		const users = collection(ctx.firestore(), 'users')
		await setDoc(doc(users, 'admin'), {
			admin: true
		})
		await setDoc(doc(users, 'horst'), {})
	})
})

describe('Parties Collection', () => {

	const scenarios = [
		{
			description: 'in unauthenticated context',
			db: () => testEnv.unauthenticatedContext().firestore()
		},
		{
			description: 'as `normal` user',
			db: () => testEnv.authenticatedContext('horst').firestore()
		},
		{
			description: 'as unknown user',
			db: () => testEnv.authenticatedContext('unknown').firestore()
		}
	]
	scenarios.forEach(({ description, db }) => {
		describe(description, () => {
			const parties = () => collection(db(), 'parties')

			it('should allow get for existing parties', async () => {
				await assertSucceeds(
					getDoc(doc(parties(), 'SEEN'))
				);
			})

			it('should not allow get for nonexistent parties', async () => {
				await assertFails(
					getDoc(doc(parties(), 'blub'))
				);
			})

			it('should not allow listing parties', async () => {
				await assertFails(
					getDocs(parties())
				);
			})

			it('should not allow creating new parties', async () => {
				await assertFails(setDoc(doc(parties(), 'test'), {
					name: 'should not work',
					greeting: 'Hello',
					members: []
				}))
			})

			it('should not allow deleting parties', async () => {
				await assertFails(deleteDoc(doc(parties(), 'test')))
			})

			it('should allow updating `last_seen` with server timestamp for existing parties',async () => {
				await assertSucceeds(updateDoc(doc(parties(), 'NEW'), {
					last_seen: serverTimestamp()
				}))
			})

			it('should not allow updating party name', async () => {
				await assertFails(updateDoc(doc(parties(), 'NEW'), {
					name: 'new name',
					last_update: serverTimestamp()
				}))
			})

			it('should not allow updating party greeting', async () => {
				await assertFails(updateDoc(doc(parties(), 'NEW'), {
					greeting: 'new greeting',
					last_update: serverTimestamp()
				}))
			})

			it('should allow updating party members when providing valid `last_update` timestamp', async () => {
				await assertSucceeds(updateDoc(doc(parties(), 'SEEN'), {
					members: ['adsajhdasl'],
					last_update: serverTimestamp()
				}))
			})

			it('should allow updating attending when providing valid `last_update` timestamp', async () => {
				await assertSucceeds(updateDoc(doc(parties(), 'SEEN'), {
					attending: true,
					last_update: serverTimestamp()
				}))
			})

			it('should allow updating finished when providing valid `last_update` timestamp', async () => {
				await assertSucceeds(updateDoc(doc(parties(), 'SEEN'), {
					finished: true,
					last_update: serverTimestamp()
				}))
			})

			it('should not allow updating finished without `last_update` timestamp', async () => {
				await assertFails(updateDoc(doc(parties(), 'SEEN'), {
					finished: true,
				}))
			})

			it('should not allow updating attending without `last_update` timestamp', async () => {
				await assertFails(updateDoc(doc(parties(), 'SEEN'), {
					attending: true,
				}))
			})

			it('should not allow updating party members without `last_update` timestamp', async () => {
				await assertFails(updateDoc(doc(parties(), 'SEEN'), {
					members: ['adsajhdasl'],
				}))
			})

			it('should not allow updating party members with wrong `last_update` timestamp', async () => {
				await assertFails(updateDoc(doc(parties(), 'SEEN'), {
					members: ['adsajhdasl'],
					last_update: Timestamp.fromDate(new Date('Jun 5, 2022, 8:23 PM'))
				}))
			})

			it('should not allow updating multiple times within short time', async () => {
				await assertSucceeds(updateDoc(doc(parties(), 'SEEN'), {
					members: ['adsajhdasl'],
					last_update: serverTimestamp()
				}))
				await assertFails(updateDoc(doc(parties(), 'SEEN'), {
					members: ['fubar'],
					last_update: serverTimestamp()
				}))
			})

			it('should not allow updating `last_seen` multiple times within short time', async () => {
				await assertSucceeds(updateDoc(doc(parties(), 'SEEN'), {
					last_seen: serverTimestamp()
				}))
				await assertFails(updateDoc(doc(parties(), 'SEEN'), {
					last_seen: serverTimestamp()
				}))
			})

			it('should allow updating multiple times with short delay', async () => {
				await assertSucceeds(updateDoc(doc(parties(), 'SEEN'), {
					members: ['adsajhdasl'],
					last_update: serverTimestamp()
				}))
				await delay(1100)
				await assertSucceeds(updateDoc(doc(parties(), 'SEEN'), {
					members: ['fubar'],
					last_update: serverTimestamp()
				}))
			}).timeout(3000)

		})
	});

	describe('as admin', () => {
		const parties = () => collection(testEnv.authenticatedContext('admin').firestore(), 'parties')

		it('should allow get for existing parties', async () => {
			await assertSucceeds(
				getDoc(doc(parties(), 'SEEN'))
			);
		})

		it('should allow delete for existing parties', async () => {
			await assertSucceeds(
				getDoc(doc(parties(), 'SEEN'))
			);
		})

		it('should allow get for nonexistent parties', async () => {
			await assertSucceeds(
				getDoc(doc(parties(), 'blub'))
			);
		})

		it('should allow listing parties', async () => {
			await assertSucceeds(
				getDocs(parties())
			);
		})
		
		it('should allow update with valid party and valid `last_update` timestamp', async () => {
			await assertSucceeds(updateDoc(doc(parties(), 'NEW'), {
				greeting: 'new greeting',
				last_update: serverTimestamp()
			}))
		})
		
		it('should not allow update without `last_update` timestamp', async () => {
			await assertFails(updateDoc(doc(parties(), 'NEW'), {
				greeting: 'new greeting',
			}))
		})

	})
})
